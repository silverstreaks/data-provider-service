package com.org.data.config;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan(basePackages="com.org.data.test")
public class TestConfig{

}
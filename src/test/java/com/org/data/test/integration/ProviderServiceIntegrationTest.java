package com.org.data.test.integration;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

import com.org.data.controller.DataController;
import com.org.data.server.DataProviderService;

import org.json.JSONArray;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

@RunWith(SpringJUnit4ClassRunner.class)
public class ProviderServiceIntegrationTest {

    @InjectMocks
    private DataController controller;

    private MockMvc mvc;

    @Mock
    private DataProviderService service;

    @Before
    public void setup() throws Exception {
        this.mvc= standaloneSetup(this.controller).build();
    }


    @Test
    public void getAllDocuments_returnJSON() {

        try {
            when(service.getData("user")).thenReturn(createJSONData());
           
           MvcResult result = mvc.perform(get("/user")
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(content()
            .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
            .andDo(print())
            .andExpect( jsonPath("$.name").value("aName"))
            .andReturn();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void getAllDocuments_return_error() {

        try {
            when(service.getData("user")).thenReturn(null);
           
           MvcResult result = mvc.perform(get("/user")
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isInternalServerError())
            .andDo(print())
            .andReturn();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private JSONArray createJSONData() {
       return new JSONArray("'{'name':'aName'}'");
    }

}
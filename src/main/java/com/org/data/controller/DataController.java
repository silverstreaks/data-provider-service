package com.org.data.controller;

import com.org.data.server.DataProviderService;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DataController {

  @Autowired
  private DataProviderService providerService;


  @RequestMapping(value = "/{collection}", method = RequestMethod.GET, produces = "application/json")
  public ResponseEntity getCollection(@PathVariable("collection") String collection) {
    
    if (StringUtils.hasText(collection)) {
      try {
       JSONArray responseArray = providerService.getData(collection);
       StringBuilder sb = new StringBuilder();
       responseArray.toList().stream().forEach( it -> sb.append(it.toString()));
       return new ResponseEntity(sb.toString(),HttpStatus.OK);
      } catch (Exception e) {
        e.printStackTrace();
        return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
      }
    }

    return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
  }
}

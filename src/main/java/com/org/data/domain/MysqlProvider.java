package com.org.data.domain;

public class MysqlProvider extends Provider{

    private static MysqlProvider mysqlProvider;

    private MysqlProvider(ProviderProperties properties){
        super.providerProperties = properties;
    }

    public static MysqlProvider getInstance(ProviderProperties props){
        if(mysqlProvider!=null){
            return mysqlProvider;
        }else{
            return new MysqlProvider(props);
        }
    }

    @Override
    public Object getClient() throws Exception {
        //Get and return mysql provider
        return null;
    }
    
}
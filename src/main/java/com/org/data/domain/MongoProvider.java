package com.org.data.domain;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;

import org.springframework.stereotype.Component;

@Component
public class MongoProvider extends Provider {

    
    private static MongoProvider mongoProvider;

    private MongoProvider(ProviderProperties properties){
        super.providerProperties = properties;
    }

    public static MongoProvider getInstance(ProviderProperties props){
        if(mongoProvider!=null){
            return mongoProvider;
        }else{
            return new MongoProvider(props);
        }
    }

    @Override
    public Object getClient() throws Exception{
            MongoClient  mongoClient = (MongoClient) MongoClients.create();
            return mongoClient.getDatabase(providerProperties.getMongodb().getDatabase());
    }
}
package com.org.data.domain;

abstract public class Provider {

protected ProviderProperties providerProperties;

abstract public Object getClient() throws Exception;

}
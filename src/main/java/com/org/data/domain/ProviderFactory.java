package com.org.data.domain;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ProviderFactory {

    @Autowired
    protected ProviderProperties providerProperties;
    
    public Provider getActiveProvider(){
        if(providerProperties.getActive().equals("mongodb")){
            return MongoProvider.getInstance(providerProperties);
        }
        return null;
    }
}
package com.org.data.domain;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.NestedConfigurationProperty;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties("provider")
public class ProviderProperties{

    private String active;

    @NestedConfigurationProperty
    private final MongoProviderProperties mongodb = new MongoProviderProperties();

    /**
     * @return the active
     */
    public String getActive() {
        return active;
    }

    /**
     * @param active the active to set
     */
    public void setActive(String active) {
        this.active = active;
    }

    /**
     * @return the mongodb
     */
    public MongoProviderProperties getMongodb() {
        return mongodb;
    }

    

    

}
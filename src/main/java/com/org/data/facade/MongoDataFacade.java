package com.org.data.facade;

import com.mongodb.Block;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

import org.bson.Document;
import org.json.JSONArray;
import org.springframework.stereotype.Repository;

@Repository
public class MongoDataFacade implements DbFacade {
    private MongoDatabase client;

    public void setClient(Object client) {
        this.client = (MongoDatabase) client;
    }

    public JSONArray fromDatabase(String collection) {
        MongoCollection<Document> dbCollection = client.getCollection(collection);
        JSONArray jsonarray = new JSONArray();
        dbCollection.find().forEach( 
            (Block<Document>)it -> jsonarray.put(it.toJson())
        );

        return jsonarray;
    }
   
}
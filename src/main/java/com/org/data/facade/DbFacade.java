package com.org.data.facade;

import org.json.JSONArray;

interface DbFacade{
    public void setClient(Object client);

    public JSONArray fromDatabase(String collection) ;
}
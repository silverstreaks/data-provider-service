package com.org.data.facade;

import org.json.JSONArray;
import org.springframework.stereotype.Repository;

@Repository
public class MysqlFacade implements DbFacade{
    private Object client;

    public void setClient(Object client){
        this.client = client;
    }

    public JSONArray fromDatabase(String table){
        // get and return JSON of data retrieved from mysql db
        return null;
    }
}
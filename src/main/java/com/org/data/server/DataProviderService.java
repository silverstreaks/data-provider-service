package com.org.data.server;

import com.mongodb.DB;
import com.mongodb.client.MongoDatabase;
import com.org.data.domain.MongoProvider;
import com.org.data.domain.MysqlProvider;
import com.org.data.domain.Provider;
import com.org.data.domain.ProviderFactory;
import com.org.data.facade.MongoDataFacade;
import com.org.data.facade.MysqlFacade;

import org.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DataProviderService {

    @Autowired
    private MongoDataFacade mongoDataFacade;

    @Autowired
    private MysqlFacade mysqlFacade;
    
    @Autowired
    private ProviderFactory providerFactory;
    
    
    public JSONArray getData(String collection) throws Exception{
        Provider activeProvider = providerFactory.getActiveProvider();
        if(activeProvider!=null){
           if(activeProvider instanceof MongoProvider){
               mongoDataFacade.setClient((MongoDatabase)activeProvider.getClient());
               return mongoDataFacade.fromDatabase(collection);
           }else if(activeProvider instanceof MysqlProvider){
               mysqlFacade.setClient((Object)activeProvider.getClient());
               return mysqlFacade.fromDatabase(collection);
           }
        }else{
            throw new Exception("No active provider");
        }
        return null;
    }



}
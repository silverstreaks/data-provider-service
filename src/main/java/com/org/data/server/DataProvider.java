package com.org.data.server;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component(value="dataProvider")
public class DataProvider {
    
    @Value( "${provider.active}" )
    private String provider;

    
    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }
    
    
}